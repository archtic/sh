# Archtic: Shell scripts

## `setup.sh`

Run this script as your main user after `archinstall` completes and you've rebooted.
```
curl -Lo- https://gitlab.com/archtic/sh/-/raw/main/setup.sh | sh
```
Installs the following:
```
* Official packages
    * base-devel                # Build tools
    * iwd                       # Wireless daemon
    * zsh                       # Shell
    * xterm                     # Fallback terminal emulator
    * alacritty                 # Preferred terminal emulator
    * i3-gaps                   # Window manager
    * polybar                   # Status bar
    * neovim                    # Editor
    * fontconfig                # Custom font support
    * ttf-dejavu                # Fallback font
    * git                       # Source control
    * lazygit                   # Git terminal UI
    * feh                       # Image viewer
    * scrot                     # Screen capture
    * htop                      # Process viewer
    * wget                      # Fetcher
    * figlet                    # Goof text
    * zoxide                    # cd alternative
    * bat                       # cat alternative
    * fzf                       # Fuzzy finder
    * tree                      # Recursive directory tree
    * fuse                      # Required for AppImages
    * man                       # Manual page viewer
    * tldr                      # Simplified manual viewer
    * glow                      # Terminal markdown viewer
    * ranger                    # Terminal file browser
    * tmux                      # Terminal multiplexer
    * xorg-xinput               # Input device configuration tool
    * openssh                   # Remote login
    * picom                     # Compositor
    * dmenu                     # dmenu
    * the_silver_searcher       # Searching tool
    * dotnet-sdk                # C# SDK
    * brightnessctl             # Display brightness tool
    * android-file-transfer     # Mount Android devices for file transfer

* Paru                          # AUR helper
    * nerd-fonts-cascadia-code  # Preferred font
    * brave-bin                 # Web browser
    * dotbare                   # Dotfiles manager (temporary)

* OhMyZsh                       # ZSH plugin framework
    * dotbare                   # Dotfile manager
    * fzf-tab                   # Fuzzy find on tab
    * zsh-autosuggestions       # Command autocomplete
    * zsh-syntax-highlighting   # Shell syntax highlighting

* nvm                           # Node version manager
    * node                      # Node.js javascript env, LTS

* AppImages
    * Bitwarden                 # Password manager
```
