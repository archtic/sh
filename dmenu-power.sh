#!/usr/bin/zsh

source ~/.zshrc

case "$(echo -e "Shutdown\nSuspend\nRestart" | dmenu \
    "${DMENU_OPTIONS[@]}" \
    -i -p 'Power:')" in
        'Shutdown') exec systemctl poweroff;;
        'Suspend') exec systemctl suspend;;
        'Restart') exec systemctl reboot;;
esac
